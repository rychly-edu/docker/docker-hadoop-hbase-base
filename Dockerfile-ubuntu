ARG HBASE_VERSION
FROM registry.gitlab.com/rychly-edu/docker/docker-hbase:${HBASE_VERSION:-latest} as hbase


ARG HADOOP_VERSION
FROM registry.gitlab.com/rychly-edu/docker/docker-hadoop-base:${HADOOP_VERSION:-latest}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

ENV \
HBASE_HOME="/opt/hbase"

COPY --from=hbase ${HBASE_HOME} ${HBASE_HOME}
COPY --from=hbase /hbase-*.sh /

ENV HBASE_CONF_DIR="${HBASE_HOME}/conf"
ENV \
HBASE_SITE_CONF="${HBASE_CONF_DIR}/hbase-site.xml" \
HBASE_POLICY_CONF="${HBASE_CONF_DIR}/hbase-policy.xml" \
HBASE_ENV_CONF="${HBASE_CONF_DIR}/hbase-env.sh" \
HBASE_USER="hbase"

RUN true \
# set up HBase permissions
&& addgroup --system "${HBASE_USER}" \
&& adduser --system --home "${HBASE_HOME}" --gecos "Apache HBase" --shell /bin/sh --ingroup "${HBASE_USER}" --disabled-login --no-create-home "${HBASE_USER}" \
&& chown -R "${HBASE_USER}:${HBASE_USER}" "${HBASE_HOME}" \
\
# set HBase path for the login shell
&& echo '#!/bin/sh' > /etc/profile.d/path-hbase.sh \
&& echo "export PATH=\"\${PATH}:${HBASE_HOME}/bin\"" >> /etc/profile.d/path-hbase.sh \
&& chmod 755 /etc/profile.d/path-hbase.sh \
\
# finish
&& true

CMD echo "This is just a base image for building another docker images!" >&2
